package com.gongj.createBeanInstance.supplier;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@ComponentScan("com.gongj.createBeanInstance")
public class AppConfig {
}
