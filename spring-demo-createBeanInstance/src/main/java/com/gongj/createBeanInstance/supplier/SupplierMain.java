package com.gongj.createBeanInstance.supplier;

import com.gongj.createBeanInstance.supplier.dto.Person;
import com.gongj.createBeanInstance.supplier.dto.User;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SupplierMain {

    // Supplier 回调
    public static void main(String[] args) {
        // 创建 BeanFactory 容器
        DefaultListableBeanFactory factory = new DefaultListableBeanFactory();
        // 构建 BeanDefinition
        RootBeanDefinition rootBeanDefinition = new RootBeanDefinition();

        // 构造方法 进行设置
        //RootBeanDefinition rootBeanDefinition = new RootBeanDefinition(Person.class,() -> new Person("gongj"));
        rootBeanDefinition.setBeanClass(User.class);
        // set方法 进行设置
        // 构造器引用
        rootBeanDefinition.setInstanceSupplier(Person::new);
        // 注册BeanDefinition
        factory.registerBeanDefinition("user",rootBeanDefinition);
        Object user = factory.getBean("user");
        // 返回的是 Person 对象
        System.out.println("结果：" +user);


        // 直接进行注册
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
        ctx.registerBean(Person.class, () -> new Person("gongj"));
        Object user1 = ctx.getBean("person");
        System.out.println(user1);

    }
}
