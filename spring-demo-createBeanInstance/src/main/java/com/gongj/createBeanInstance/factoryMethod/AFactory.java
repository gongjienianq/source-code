package com.gongj.createBeanInstance.factoryMethod;

public class AFactory {
    private static A a;

    //静态方法 创建 A 对象
    public static A getInstance(String name){
        return  new A(name);
    }

    //实例工厂注入
    public A getInstanceMethod(String name){
        return new A(name);
    }
}
