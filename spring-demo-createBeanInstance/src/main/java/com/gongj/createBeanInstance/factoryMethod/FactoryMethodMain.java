package com.gongj.createBeanInstance.factoryMethod;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class FactoryMethodMain {


    // 实例工厂的方式
    public static void main(String[] args) {
        ClassPathXmlApplicationContext cxt = new ClassPathXmlApplicationContext("factory-method.xml");
        A a = (A)cxt.getBean("a");
        System.out.println("结果：" +a.hashCode()+ "===" + a);
        // 第二次会从缓存中获取 重复调用 getBean 方法并且不传参
        Object a1= cxt.getBean("a");
        System.out.println("结果：" +a1.hashCode()+ "===" + a1);
        //  重复调用 getBean 方法并且传参
        Object a2= cxt.getBean("a","哈哈哈哈");
        System.out.println("结果：" +a2.hashCode() + "===" + a2);
    }

    // 静态工厂方式
    public static void main2(String[] args) {
        ClassPathXmlApplicationContext cxt = new ClassPathXmlApplicationContext("factory-method.xml");
        A aFactory = (A)cxt.getBean("aFactory");
        System.out.println("结果：" +aFactory.hashCode() + "===" + aFactory);

        // 第二次会从缓存中获取 重复调用 getBean 方法并且不传参
        A aFactory1 = (A)cxt.getBean("aFactory");
        System.out.println("结果：" +aFactory1.hashCode() + "===" + aFactory);

        //  重复调用 getBean 方法并且传参
        A aFactory2 = (A)cxt.getBean("aFactory","hieghieghei");
        System.out.println("结果：" +aFactory2.hashCode() + "===" + aFactory2);
    }
}