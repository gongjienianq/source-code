package com.gongj.createBeanInstance.factoryMethod;

public class A {

    private String name;

    private String email;

    public A() {
    }

    public A(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "A{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}

