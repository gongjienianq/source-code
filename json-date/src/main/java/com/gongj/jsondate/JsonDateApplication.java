package com.gongj.jsondate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsonDateApplication {

    public static void main(String[] args) {
        SpringApplication.run(JsonDateApplication.class, args);
    }

}
