package com.gongj.jsondate.controller;

import com.gongj.jsondate.dto.UserDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
public class UserController {

    @GetMapping("/getUser")
    public List<UserDTO> getUser() {
        List<UserDTO> userList = new ArrayList<UserDTO>();
        for (int i=1; i<=3; i++) {
            UserDTO user = new UserDTO();
            user.setCreateTime(new Date());
            user.setUsername("gongj" + i);
            user.setId("j" + i);
            userList.add(user);
        }
        return userList;
    }

}
