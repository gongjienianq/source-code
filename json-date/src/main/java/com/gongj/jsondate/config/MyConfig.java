package com.gongj.jsondate.config;

import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.text.SimpleDateFormat;

//@Configuration
public class MyConfig {

//    @Bean
//    MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverterConfiguration() {
//        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
//        ObjectMapper om = new ObjectMapper();
//        //全局修改josn时间格式
//        om.setDateFormat(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"));
//        converter.setObjectMapper(om);
//        return converter;
//    }

//    @Bean
//    ObjectMapper objectMapper() {
//        ObjectMapper om = new ObjectMapper();
//        om.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
//        return om;
//    }

//    @Bean
//    FastJsonHttpMessageConverter FastJsonHttpMessageConverter() {
//        FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
//        FastJsonConfig config = new FastJsonConfig();
//        config.setDateFormat("yyyy-MM-dd HH");
//        converter.setFastJsonConfig(config);
//        return converter;
//    }
}
